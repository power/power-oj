import Vue from 'vue'
import axios from 'axios'
import Vuetable from './components/table/Vuetable.vue'
import App from './App.vue'
import router from './router'
import auth from './services/Auth'

require('semantic-ui-css/semantic.min.css')
require('semantic-ui-css/semantic.min.js')

Vue.config.productionTip = false

auth.checkAuth()

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || 'Power OJ'
  if (to.meta.auth && !auth.loggedIn()) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else if (to.meta.guest && auth.loggedIn()) {
    next('/')
  } else {
    next()
  }
})

axios.interceptors.response.use(function (response) {
  if (response.status === 401 && !auth.loggedIn()) {
    router.push('/login')
  }
  return response
}, function (error) {
  return Promise.reject(error)
})

Vue.use(Vuetable)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
