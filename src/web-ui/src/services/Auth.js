import axios from 'axios'
import router from '../router'

export default {

  user: {
    name: '',
    authorities: [],
    isAdmin: false,
    authenticated: false
  },

  login (username, password, cb) {
    axios.post('/login', `username=${username}&password=${password}`)
      .then(() => {
        this.getUserInfo()
        if (cb) {
          cb(true)
        }
      }, () => {
        if (cb) {
          cb(false)
        }
      })
  },

  getUserInfo () {
    axios.get('/principal').then((response) => {
      let auth = response.data
      if (auth) {
        this.user.name = auth.name
        this.user.authorities = auth.authorities
        this.user.isAdmin = auth.authorities.findIndex(e => e['authority'] === 'ROLE_ADMIN') > -1
        this.user.authenticated = true
      }
    })
  },

  loggedIn () {
    return this.user.authenticated
  },

  checkAuth () {
    this.getUserInfo()
  },

  logout () {
    axios.post('/logout').then(() => {
      this.user.name = ''
      this.user.authorities = []
      this.user.isAdmin = false
      this.user.authenticated = false
      router.push('/')
    })
  },

}
