import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import ProblemList from './views/ProblemList.vue'
import UserList from './views/UserList.vue'
import Login from './views/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,

    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/problems',
      name: 'problems',
      component: ProblemList
    },
    {
      path: '/users',
      name: 'users',
      component: UserList
    },
    {
      path: '/login',
      component: Login,
      meta: { guest: true }
    },
  ],
  linkActiveClass: 'active'
})
