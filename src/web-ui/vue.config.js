const webpack = require('webpack')
const path = require('path')

module.exports = {
  outputDir: path.resolve(__dirname, '../main/resources/static'),
  devServer: {
    proxy: {
      '/users': {
        target: 'http://localhost:8080',
        ws: true,
        changeOrigin: true
      },
      '/problems': {
        target: 'http://localhost:8080',
        ws: true,
        changeOrigin: true
      },
      '/login': {
        target: 'http://localhost:8080',
        ws: true,
        changeOrigin: true
      },
      '/logout': {
        target: 'http://localhost:8080',
        ws: true,
        changeOrigin: true
      },
      '/principal': {
        target: 'http://localhost:8080',
        ws: true,
        changeOrigin: true
      },
    }
  },
  chainWebpack: config => {
    config
      .plugin('provide')
      .use(webpack.ProvidePlugin, [{
        $: 'jquery',
        jquery: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      }])
  },
}
