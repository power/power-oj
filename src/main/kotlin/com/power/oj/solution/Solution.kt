package com.power.oj.solution

import com.power.oj.judge.JudgeStatus
import com.power.oj.problem.Problem
import com.power.oj.user.User
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Solution(
    @Column(nullable = false, columnDefinition = "TEXT") val code: String,
    val language: String,
    @ManyToOne @JoinColumn val user: User,
    @ManyToOne @JoinColumn val problem: Problem,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int? = null
) {
    var timeUsage: Int = 0
    var memoryUsage: Int = 0
    var status: JudgeStatus = JudgeStatus.WAIT
    var error: String = ""

    @CreatedDate
    val createdTime: LocalDateTime = LocalDateTime.now()
}
