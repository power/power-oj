package com.power.oj.solution

import com.power.oj.problem.ProblemService
import com.power.oj.user.UserService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.access.AccessDeniedException
import org.springframework.stereotype.Service

@Service
class SolutionService(
    private val repository: SolutionRepository,
    private val userService: UserService,
    private val problemService: ProblemService
) {
    fun getSolutions(pageable: Pageable): Page<Solution> {
        return repository.findAll(pageable)
    }

    fun getSolutionById(id: Int): Solution {
        return repository.getOne(id)
    }

    fun createSolution(dto: SolutionDto): Solution {
        val user = userService.getCurrentUser() ?: throw AccessDeniedException("User does not login")
        val problem = problemService.getProblemById(dto.problemId)
        val solution = Solution(dto.code, dto.language, user, problem)
        return repository.save(solution)
    }

}
