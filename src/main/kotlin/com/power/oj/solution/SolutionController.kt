package com.power.oj.solution

import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/solutions")
class SolutionController(private val service: SolutionService) {

    @GetMapping
    fun getSolutions(pageable: Pageable) = service.getSolutions(pageable)

    @GetMapping("/{id}")
    fun getSolutionById(@PathVariable id: Int) = service.getSolutionById(id)

    @PostMapping
    fun createSolution(@RequestBody dto: SolutionDto) = service.createSolution(dto)

}
