package com.power.oj.solution

data class SolutionDto(
    val code: String,
    val language: String,
    val problemId: Int
)
