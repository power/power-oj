package com.power.oj

import com.power.oj.problem.Problem
import com.power.oj.problem.ProblemRepository
import com.power.oj.user.Role
import com.power.oj.user.User
import com.power.oj.user.UserRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.password.PasswordEncoder

@SpringBootApplication
class PowerOjApplication {

    @Bean
    @Profile("dev")
    fun databaseInitializer(
        userRepository: UserRepository,
        problemRepository: ProblemRepository,
        passwordEncoder: PasswordEncoder
    ) = CommandLineRunner {
        val admin = User("admin", "admin", passwordEncoder.encode("admin"), Role.ROLE_ADMIN, "admin@har01d.com")
        userRepository.save(admin)

        val user = User("user", "user", passwordEncoder.encode("user"), email = "user@har01d.com")
        userRepository.save(user)

        for (i in 1..20) {
            val u = User("user$i", "user-$i", passwordEncoder.encode("user"), email = "user$i@har01d.com")
            userRepository.save(u)
        }

        val problem = Problem("A+B", "Calculate a+b", "Two integer a,b.", "Output a+b", "1 2", "3", author = admin)
        problemRepository.save(problem)
    }

}

fun main(args: Array<String>) {
    runApplication<PowerOjApplication>(*args)
}
