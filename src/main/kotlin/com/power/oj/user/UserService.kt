package com.power.oj.user

import com.power.oj.core.exception.NotFoundException
import com.power.oj.core.exception.ValidationException
import org.springframework.data.domain.Pageable
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class UserService(
    private val repository: UserRepository,
    private val passwordEncoder: PasswordEncoder
) {

    fun getCurrentUser(): User? {
        val authentication = SecurityContextHolder.getContext().authentication
        if (authentication != null) {
            val username = authentication.name
            return repository.findByUsername(username)
        }
        return null
    }

    fun getUsers(pageable: Pageable) = repository.findAll(pageable)

    fun getUserByName(name: String) = repository.findByUsername(name)

    fun createUser(dto: UserDto): User {
        validateUnique(dto, null)
        val user =
            User(
                dto.username,
                dto.nickname,
                passwordEncoder.encode(dto.password),
                Role.ROLE_USER,
                dto.email,
                dto.mobile,
                dto.qq,
                dto.wechat,
                dto.weibo
            )
        user.avatar = dto.avatar
        user.signature = dto.signature
        user.homepage = dto.homepage
        user.school = dto.school
        user.company = dto.company
        return repository.save(user)
    }

    fun updateUser(name: String, dto: UserDto): User {
        val user = repository.findByUsername(name) ?: throw NotFoundException("User $name does not exist")
        validateUnique(dto, user)
        user.email = dto.email
        user.mobile = dto.mobile
        user.qq = dto.qq
        user.wechat = dto.wechat
        user.weibo = dto.weibo
        user.nickname = dto.nickname
        user.avatar = dto.avatar
        user.signature = dto.signature
        user.homepage = dto.homepage
        user.school = dto.school
        user.company = dto.company
        user.lastModifiedTime = LocalDateTime.now()
        return repository.save(user)
    }

    private fun validateUnique(dto: UserDto, user: User?) {
        if (user == null) {
            if (repository.existsByUsername(dto.username)) {
                throw ValidationException("Username was registered")
            }
            if (dto.email != null && repository.existsByEmail(dto.email)) {
                throw ValidationException("Email was registered")
            }
            if (dto.mobile != null && repository.existsByMobile(dto.mobile)) {
                throw ValidationException("Mobile was registered")
            }
            if (dto.qq != null && repository.existsByQq(dto.qq)) {
                throw ValidationException("QQ was registered")
            }
            if (dto.wechat != null && repository.existsByWechat(dto.wechat)) {
                throw ValidationException("Wechat was registered")
            }
            if (dto.weibo != null && repository.existsByWeibo(dto.weibo)) {
                throw ValidationException("Weibo was registered")
            }
            if (repository.existsByNickname(dto.nickname)) {
                throw ValidationException("Nickname was registered")
            }
        } else {
            if (dto.email != null && dto.email != user.email && repository.existsByEmail(dto.email)) {
                throw ValidationException("Email was registered")
            }
            if (dto.mobile != null && dto.mobile != user.mobile && repository.existsByMobile(dto.mobile)) {
                throw ValidationException("Mobile was registered")
            }
            if (dto.qq != null && dto.qq != user.qq && repository.existsByQq(dto.qq)) {
                throw ValidationException("QQ was registered")
            }
            if (dto.wechat != null && dto.wechat != user.wechat && repository.existsByWechat(dto.wechat)) {
                throw ValidationException("Wechat was registered")
            }
            if (dto.weibo != null && dto.weibo != user.weibo && repository.existsByWeibo(dto.weibo)) {
                throw ValidationException("Weibo was registered")
            }
            if (dto.nickname != user.nickname && repository.existsByNickname(dto.nickname)) {
                throw ValidationException("Nickname was registered")
            }
        }
    }

}
