package com.power.oj.user

data class UserDto(
    val username: String,
    val password: String,
    val email: String? = null,
    val mobile: String? = null,
    val qq: String? = null,
    val wechat: String? = null,
    val weibo: String? = null,
    val nickname: String = username,
    val avatar: String? = null,
    val signature: String? = null,
    val homepage: String? = null,
    val school: String? = null,
    val company: String? = null
)
