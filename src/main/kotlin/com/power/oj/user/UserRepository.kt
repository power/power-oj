package com.power.oj.user

import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Int> {
    fun findByUsername(username: String): User?
    fun existsByUsername(username: String): Boolean
    fun existsByEmail(email: String): Boolean
    fun existsByMobile(mobile: String): Boolean
    fun existsByQq(qq: String): Boolean
    fun existsByWechat(wechat: String): Boolean
    fun existsByWeibo(weibo: String): Boolean
    fun existsByNickname(nickname: String): Boolean
}
