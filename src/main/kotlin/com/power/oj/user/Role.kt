package com.power.oj.user

enum class Role {
    ROLE_ADMIN,
    ROLE_USER,
}
