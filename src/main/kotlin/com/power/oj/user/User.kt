package com.power.oj.user

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class User(
    @Column(nullable = false, unique = true, length = 35) val username: String,
    @Column(unique = true, length = 35) var nickname: String = username,
    @JsonIgnore var password: String,
    @Enumerated(EnumType.STRING) var role: Role = Role.ROLE_USER,
    @Column(unique = true, length = 35) var email: String? = null,
    @Column(unique = true, length = 20) var mobile: String? = null,
    @Column(unique = true, length = 20) var qq: String? = null,
    @Column(unique = true, length = 35) var wechat: String? = null,
    @Column(unique = true, length = 35) var weibo: String? = null,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int = 0
) {
    var avatar: String? = null
    var signature: String? = null
    var homepage: String? = null
    var school: String? = null
    var company: String? = null

    var rank: Int = 0
    var solved: Int = 0
    var submissions: Int = 0

    var enabled = true
    var expired = false
    var locked = false
    var credentialsExpired = false

    @CreatedDate
    val createdTime = LocalDateTime.now()

    @JsonIgnore
    @LastModifiedDate
    var lastModifiedTime: LocalDateTime? = null

    var lastLoginTime: LocalDateTime? = null
}
