package com.power.oj.user

import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/users")
class UserController(private val service: UserService) {

    @GetMapping
    fun getUsers(pageable: Pageable) = service.getUsers(pageable)

    @GetMapping("/{name}")
    fun getUserByName(@PathVariable name: String) = service.getUserByName(name)

    @PostMapping
    fun createUser(@RequestBody dto: UserDto) = service.createUser(dto)

    @PostMapping("/{name}")
    fun updateUser(@PathVariable name: String, @RequestBody dto: UserDto) = service.updateUser(name, dto)

}
