package com.power.oj

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/")
class HomeController {

    @GetMapping("/principal")
    fun principal(principal: Principal?) = principal

}
