package com.power.oj.core.security

import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class AccountAuthenticationProvider(
    private val userDetailsService: UserDetailsService,
    private val passwordEncoder: PasswordEncoder
) : AbstractUserDetailsAuthenticationProvider() {

    override fun additionalAuthenticationChecks(userDetails: UserDetails, token: UsernamePasswordAuthenticationToken) {
        if (token.credentials == null || userDetails.password == null) {
            throw BadCredentialsException("Credentials may not be null.")
        }

        if (!passwordEncoder.matches(token.credentials as String, userDetails.password)) {
            throw BadCredentialsException("Bad credentials")
        }
    }

    override fun retrieveUser(username: String, token: UsernamePasswordAuthenticationToken) =
        userDetailsService.loadUserByUsername(username)

}
