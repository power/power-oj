package com.power.oj.core.security

import com.power.oj.user.UserRepository
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CustomAuthenticationSuccessHandler(private val userRepository: UserRepository) : AuthenticationSuccessHandler {
    override fun onAuthenticationSuccess(
        request: HttpServletRequest,
        response: HttpServletResponse,
        authentication: Authentication
    ) {
        val user = userRepository.findByUsername(authentication.name)
        user!!.lastLoginTime = LocalDateTime.now()
        userRepository.save(user)
        response.status = HttpServletResponse.SC_OK
    }
}
