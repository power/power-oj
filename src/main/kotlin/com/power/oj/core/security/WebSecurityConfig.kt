package com.power.oj.core.security

import com.power.oj.user.UserRepository
import org.springframework.context.annotation.Bean
import org.springframework.security.access.hierarchicalroles.RoleHierarchy
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.AuthenticationEntryPoint
import javax.servlet.http.HttpServletResponse

@EnableWebSecurity
class WebSecurityConfig(private val userRepository: UserRepository) : WebSecurityConfigurerAdapter() {

    @Bean
    override fun userDetailsService() = CustomUserDetailsService(userRepository)

    @Bean
    fun passwordEncoder() = BCryptPasswordEncoder()

    @Bean
    fun authenticationProvider(
        userDetailsService: UserDetailsService,
        passwordEncoder: PasswordEncoder
    ) = AccountAuthenticationProvider(userDetailsService, passwordEncoder)

    @Bean
    fun authenticationEntryPoint() = AuthenticationEntryPoint { _, response, _ ->
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized")
    }

    @Bean
    fun roleHierarchy(): RoleHierarchy {
        val roleHierarchyImpl = RoleHierarchyImpl()
        roleHierarchyImpl.setHierarchy("ROLE_ADMIN > ROLE_USER")
        return roleHierarchyImpl
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(AccountAuthenticationProvider(userDetailsService(), passwordEncoder()))
    }

    override fun configure(http: HttpSecurity) {
        http
            .authorizeRequests()
            .anyRequest().permitAll()
            .and()
            .formLogin()
            .successHandler(CustomAuthenticationSuccessHandler(userRepository))
            .failureHandler(CustomAuthenticationFailureHandler())
            .and()
            .logout().logoutSuccessHandler { _, response, _ -> response.status = HttpServletResponse.SC_OK }
            .and()
            .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
            .and()
            .csrf().disable()
            .sessionManagement().maximumSessions(1)
    }

}
