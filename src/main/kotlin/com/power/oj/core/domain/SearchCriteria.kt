package com.power.oj.core.domain

data class SearchCriteria(val text: String, val scope: String)
