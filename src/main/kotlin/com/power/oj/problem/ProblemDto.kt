package com.power.oj.problem

data class ProblemDto(
    val title: String,
    val description: String,
    val input: String = "",
    val output: String = "",
    val sampleInput: String = "",
    val sampleOutput: String = "",
    val hint: String = "",
    val source: String = "",
    val sampleProgram: String = "",
    val timeLimit: Int = 1000,
    val memoryLimit: Int = 65536,
    val outputLimit: Int = 65536,
    val enabled: Boolean = true
)
