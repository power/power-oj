package com.power.oj.problem

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface ProblemRepository : JpaRepository<Problem, Int> {
    fun findAllByEnabledTrue(pageable: Pageable): Page<Problem>
    fun findAllByEnabledTrueAndTitleContains(text: String, pageable: Pageable): Page<Problem>
}
