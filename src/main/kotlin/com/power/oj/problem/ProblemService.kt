package com.power.oj.problem

import com.power.oj.core.exception.NotFoundException
import com.power.oj.user.UserService
import org.springframework.data.domain.Pageable
import org.springframework.security.access.AccessDeniedException
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class ProblemService(
    private val repository: ProblemRepository,
    private val userService: UserService
) {

    fun getProblems(pageable: Pageable) = repository.findAllByEnabledTrue(pageable)

    fun getProblemById(id: Int): Problem {
        val problem = repository.findById(id)
        if (!problem.isPresent || !problem.get().enabled) {
            throw NotFoundException("Problem $id does not exist")
        }
        return problem.get()
    }

    fun searchProblemByTitle(text: String, pageable: Pageable) =
        repository.findAllByEnabledTrueAndTitleContains(text, pageable)

    fun addProblem(dto: ProblemDto): Problem {
        val user = userService.getCurrentUser() ?: throw AccessDeniedException("User does not login")
        val problem = Problem(
            dto.title, dto.description, dto.input, dto.output, dto.sampleInput,
            dto.sampleOutput, dto.hint, dto.source, dto.sampleProgram, dto.timeLimit,
            dto.memoryLimit, dto.outputLimit, user
        )
        return repository.save(problem)
    }

    fun updateProblem(problem: Problem): Problem {
        problem.lastModifiedTime = LocalDateTime.now()
        return repository.save(problem)
    }

}
