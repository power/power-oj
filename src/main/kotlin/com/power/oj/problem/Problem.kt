package com.power.oj.problem

import com.fasterxml.jackson.annotation.JsonIgnore
import com.power.oj.user.User
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Problem(
    @Column(nullable = false) var title: String,
    @Column(nullable = false, columnDefinition = "TEXT") var description: String,
    @Column(nullable = false, columnDefinition = "TEXT") var input: String = "",
    @Column(nullable = false, columnDefinition = "TEXT") var output: String = "",
    @Column(nullable = false, columnDefinition = "TEXT") var sampleInput: String = "",
    @Column(nullable = false, columnDefinition = "TEXT") var sampleOutput: String = "",
    @Column(nullable = false, columnDefinition = "TEXT") var hint: String = "",
    @Column(nullable = false, columnDefinition = "TEXT") var source: String = "",
    @Column(nullable = false, columnDefinition = "TEXT") var sampleProgram: String = "",
    var timeLimit: Int = 1000,
    var memoryLimit: Int = 65536,
    var outputLimit: Int = 65536,
    @ManyToOne @JoinColumn val author: User,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int? = null
) {
    var enabled = true
    var accepted: Int = 0
    var solved: Int = 0
    var submissions: Int = 0
    var submitUser: Int = 0

    @CreatedDate
    val createdTime: LocalDateTime = LocalDateTime.now()

    @JsonIgnore
    @LastModifiedDate
    var lastModifiedTime: LocalDateTime? = null
}
