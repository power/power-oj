package com.power.oj.problem

import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/problems")
class ProblemController(private val service: ProblemService) {

    @GetMapping
    fun getProblems(pageable: Pageable) = service.getProblems(pageable)

    @GetMapping("/{id}")
    fun getProblemById(@PathVariable id: Int) = service.getProblemById(id)

    @GetMapping("/search")
    fun searchProblemByTitle(@RequestParam("q") text: String, pageable: Pageable) =
        service.searchProblemByTitle(text, pageable)

//    @GetMapping("/statistics")
//    fun getProblemStatistics() {}

    @PostMapping
    fun addProblem(@RequestBody problem: ProblemDto) = service.addProblem(problem)

    @PostMapping("/{id}")
    fun updateProblem(@PathVariable id: Int, @RequestBody problem: Problem) = service.updateProblem(problem)

}
