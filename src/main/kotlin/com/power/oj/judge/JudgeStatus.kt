package com.power.oj.judge

enum class JudgeStatus(private val description: String) {
    AC("Accepted"),
    PE("Presentation Error"),
    WA("Wrong Answer"),
    TLE("Time Limit Exceed"),
    MLE("Memory Limit Exceed"),
    OLE("Output Limit Exceed"),
    CE("Compile Error"),
    RF("Restricted Function"),
    RE("Runtime Error"),
    SE("System Error"),
    VE("Validate Error"),
    RUN("Running"),
    WAIT("Waiting"),
    REJUDGE("Rejudging"),
    SIM("Similar"),
    COMPILE("Compiling"),
    QUEUE("Queuing"), ;

    override fun toString(): String {
        return description
    }
}
